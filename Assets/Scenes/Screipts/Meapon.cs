using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private float delay = 0.6f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    private bool isReadyToShoot = true;

    private void Update()
    {
        if (Input.GetButton("Fire1") && isReadyToShoot)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        PlayMuzzleFlash();
        Raycasting();
        StartCoroutine(DeleyCountdown(delay));
    }

    private void PlayMuzzleFlash()
    {
        muzzleFlashEffect.Play();
    }

    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        Debug.Log("� ����� � " + hit.transform.name);
        Health targetHealth = hit.transform.GetComponent<Health>();
        if (targetHealth == null) return;
        targetHealth.TakeDamage(damage);
    }

    private IEnumerator DeleyCountdown(float delay)
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(delay);
        isReadyToShoot = true;
    }
}