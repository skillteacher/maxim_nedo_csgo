using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float healthAmout = 100f;

    public void TakeDamage(float damageAmount)
    {
        healthAmout -= damageAmount;

        if(healthAmout <= 0)
        {
            Destroy(gameObject);
        }

    }
}