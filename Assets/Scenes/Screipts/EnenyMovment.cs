using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnenyMovment : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float visionRange = 10f;
    private NavMeshAgent navMeshAgent;
    private bool isRaged;
    private float distanceToTarget;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        distanceToTarget = Vector3.Distance(target.position, transform.position);

        if (isRaged)
        {
            EngageTarget();
        }
        else if (visionRange >= distanceToTarget)
        {
            isRaged = true;
        }
    }

    private void EngageTarget()
    {
        if (navMeshAgent.stoppingDistance >= distanceToTarget)
        {
            AttackTarget();
        }
        else
        {
            ChaseTarget();
        }
    }

    private void ChaseTarget()
    {
        navMeshAgent.SetDestination(target.position);
    }

    private void AttackTarget()
    {
        Debug.Log(name+" Attack " + target.name);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, visionRange);  
    }
}